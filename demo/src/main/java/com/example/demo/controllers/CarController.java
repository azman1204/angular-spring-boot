package com.example.demo.controllers;

import com.example.demo.dto.request.OwnerDTO;
import com.example.demo.models.OwnerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CarController {
    // dependency injection
    @Autowired
    OwnerRepo ownerRepo;

    // http://localhost:8080/cars
    @RequestMapping("/cars")
    public String getCars() {
        return "Hello World"; // return sbg text/plain
    }

    // http://localhost:8080/brands
    @GetMapping("/brands")
    public ArrayList<String> getBrands() {
        ArrayList<String> brands = new ArrayList();
        brands.add("Tesla");
        brands.add("Toyota");
        brands.add("Proton");
        return brands; // SP auto convert obj kpd application/JSON
        // output : ["Tesla", "Toyota", "Proton"]
    }

    @GetMapping("/owners")
    public ArrayList<Owner> getOwners() {
        ArrayList<Owner> owners = new ArrayList();
        Owner o1 = new Owner();
        o1.id = 1;
        o1.name = "John Doe";
        owners.add(o1);

        Owner o2 = new Owner();
        o2.id = 2;
        o2.name = "Abu Bakar Ellah";
        owners.add(o2);

        return owners;
        // output [{"id":1, "name":"JohnDoe"}, {"id":2, "name":"Abu Bakar Ellah"}]
    }

    @GetMapping("/owners2")
    public ArrayList<Owner> getOwners2() {
        ArrayList<Owner> owners = new ArrayList<Owner>();
        String url = "jdbc:mysql://localhost:3307/demo";
        String user = "root";
        String pwd = "";
        try {
            Connection conn = DriverManager.getConnection(url, user, pwd);
            Statement stmt =conn.createStatement();
            String sql = "SELECT * FROM owner";
            ResultSet rs = stmt.executeQuery(sql);
            while(rs.next()) {
                Owner owner = new Owner();
                owner.id = rs.getInt("id");
                owner.name = rs.getString("name");
                owners.add(owner);
            }
        } catch(SQLException exception) {
            System.out.println("Error " + exception);
        }
        return owners;
    }

    @GetMapping("/owners3")
    public Iterable<com.example.demo.models.Owner> getOwners3() {
        return ownerRepo.findAll();
    }

    @GetMapping("/owners3/{id}")
    public com.example.demo.models.Owner getOneOwner(@PathVariable Long id) {
        Optional<com.example.demo.models.Owner> opt = ownerRepo.findById(id);

        if (opt.isPresent())
            return opt.get();

        return new com.example.demo.models.Owner();
    }

    // example POST data using body
    @PostMapping("/owners")
    public com.example.demo.models.Owner insert(@RequestBody OwnerDTO owner) {
        com.example.demo.models.Owner owner2 = new com.example.demo.models.Owner();
        owner2.setName(owner.getName());
        ownerRepo.save(owner2);
        return owner2;
    }

    // PUT - update semua data, PATCH - update sebahagian data
    @PutMapping("/owners/{id}")
    public ResponseEntity<String> update(@PathVariable Long id, @RequestBody OwnerDTO ownerDTO) {
        Optional<com.example.demo.models.Owner> opt = ownerRepo.findById(id);
        if (opt.isPresent()) {
            // update disini
            com.example.demo.models.Owner owner = opt.get();
            owner.setName(ownerDTO.getName());
            ownerRepo.save(owner);
            return ResponseEntity.ok("Data updated"); // 200
        } else {
            // id tidak wujud
            return new ResponseEntity(HttpStatus.BAD_REQUEST); // 400
        }
    }

    @DeleteMapping("/owners/{id}")
    public ResponseEntity<String> delete(@PathVariable Long id) {
        Optional<com.example.demo.models.Owner> opt = ownerRepo.findById(id);
        if (!opt.isPresent()) {
            // owner tidak wujud
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Owner tidak ditemui");
        }

        ownerRepo.deleteById(id);
        return ResponseEntity.ok("Data deleted");
    }
}

class Owner {
    public String name;
    public Integer id;
}