package com.example.demo.controllers;

import com.example.demo.dto.request.UserDTO;
import com.example.demo.dto.response.Success;
import com.example.demo.models.User;
import com.example.demo.models.UserRepo;
import com.example.demo.service.JwtService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    @Autowired
    UserRepo userRepo;

    private final JwtService jwtService; // DI

    // register user
    @PostMapping("/register")
    public User register(@RequestBody UserDTO userDTO) {
        User user = new User();
        user.setName(userDTO.getName());
        String hashed = BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt());
        user.setPassword(hashed);
        user.setRole(userDTO.getRole());
        user.setUserId(userDTO.getUserId());
        userRepo.save(user);
        return user;
    }

    // update password
    @PatchMapping("/update-password/{id}")
    public ResponseEntity<String> updatePassword(@PathVariable Long id, @RequestBody UserDTO userDTO) {
        Optional<User> opt = userRepo.findById(id);
        if (opt.isPresent()) {
            User user = opt.get();
            String hashed = BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt());
            user.setPassword(hashed);
            userRepo.save(user);
            return ResponseEntity.ok("Password updated");
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Tak dapat kemaskini password");
        }
    }

    // login
    @PostMapping("/login")
    public ResponseEntity<Success> login(@RequestBody UserDTO userDTO) {
        User user = userRepo.findByUserId(userDTO.getUserId());

        if (user == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "user tidak wujud");
        }

        boolean ok = BCrypt.checkpw(userDTO.getPassword(), user.getPassword());
        if (ok) {
            // berjaya login
            String token = jwtService.generateToken(user);
            Success s = new Success();
            s.setMessage(token);
            return ResponseEntity.ok(s);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Password salah");
        }
    }
}
