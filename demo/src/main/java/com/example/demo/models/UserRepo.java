package com.example.demo.models;

import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepo extends CrudRepository<User, Long> {
    public User findByUserId(String userId);
}
