import { CanActivateFn } from '@angular/router';

// function ini akan return true/false
// true - boleh masuk, false - tak boleh masuk.
// biasa decision based on auth dan authorization
export const authGuard: CanActivateFn = (route, state) => {
  let role = localStorage.getItem('role');
  if (role === 'admin') {
    return true;
  } else {
    return false;
  }
};
