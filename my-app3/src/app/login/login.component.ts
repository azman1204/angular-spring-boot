import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { LoginService } from '../services/login.service';
import { jwtDecode, JwtPayload } from 'jwt-decode';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  username ='';
  password = '';

  constructor(private loginService: LoginService) {}

  login() {
    console.log("username = " + this.username);
    console.log("password = " + this.password);
    this.loginService.login(this.username, this.password)
        .subscribe(data => {
          console.log(data);
          let decoded = jwtDecode<JwtLoad>(data.message);
          console.log(decoded.role);
          localStorage.setItem("jwt", data.message);
          localStorage.setItem("role", decoded.role);
          localStorage.setItem("userId", decoded.sub);
        });
  }
}

interface JwtLoad {
  sub: string;
  role: string;
}
