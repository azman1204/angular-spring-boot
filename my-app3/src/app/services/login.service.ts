import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Message } from '../models/message';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient: HttpClient) { }

  login(userid: string, password: string) : Observable<Message> {
    let url = 'http://localhost:8080/user/login';
    return this.httpClient.post<Message>(url, {userId: userid, password: password});
  }
}
