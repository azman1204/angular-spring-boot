import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Owner } from '../models/owner';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CarOwnerService {

  constructor(private http:HttpClient) { }

  getOwners() : Observable<Owner[]> {
    let jwt = localStorage.getItem('jwt');
    let headers = new HttpHeaders({
      'Authorization': 'Bearer ' + jwt
    });
    return this.http.get<Owner[]>('http://localhost:8080/owners', {headers: headers});
  }
}
