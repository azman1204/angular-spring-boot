import { Component } from '@angular/core';
import { ReactiveFormsModule, FormGroup, Validators, FormControl } from '@angular/forms';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-registration',
  standalone: true,
  imports: [ReactiveFormsModule, CommonModule],
  templateUrl: './registration.component.html',
  styleUrl: './registration.component.css'
})
export class RegistrationComponent {
  myForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.pattern('ab[0-9]+')]),
    email: new FormControl('abu@gmail.com', [Validators.email, Validators.required])
  });

  doSubmit() {
    console.log(this.myForm.value);
  }
}
