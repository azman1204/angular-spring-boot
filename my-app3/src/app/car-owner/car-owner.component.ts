import { Component, OnInit } from '@angular/core';
import { CarOwnerService } from '../services/car-owner.service';
import { Owner } from '../models/owner';

@Component({
  selector: 'app-car-owner',
  standalone: true,
  imports: [],
  templateUrl: './car-owner.component.html',
  styleUrl: './car-owner.component.css'
})
export class CarOwnerComponent implements OnInit {
  owners: Owner[] = [];

  constructor(private carOwnerService: CarOwnerService) {}

  ngOnInit(): void {
      this.carOwnerService.getOwners().subscribe(data => {
        this.owners = data;
      });
  }
}
