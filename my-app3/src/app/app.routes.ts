import { Routes } from '@angular/router';
import { Demo1Component } from './demo1/demo1.component';
import { Demo2Component } from './demo2/demo2.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { CarOwnerComponent } from './car-owner/car-owner.component';
import { authGuard } from './pakguard/auth.guard';

export const routes: Routes = [
  {path: '', component:Demo1Component},
  {path: 'demo1', component: Demo1Component, canActivate: [authGuard]},
  {path: 'demo2',component: Demo2Component},
  {path: 'test', redirectTo: 'demo1'},
  {path: 'login', component: LoginComponent},
  {path: 'registration', component: RegistrationComponent},
  {path: 'car-owner', component: CarOwnerComponent},
  {path: '**', component: NotFoundComponent}
];
