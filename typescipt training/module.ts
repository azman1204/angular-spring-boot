import { Car, Tesla } from "./interface_class";

class S90 implements Car {
    name: string = "S90";
    releaseYear: number = 2023;
    info(): string {
        return `name = ${this.name}`;
    }
}

let tesla = new Tesla("Tesla Model S", 2024);
console.log(tesla.info());

let s90 = new S90();
console.log(s90.info());