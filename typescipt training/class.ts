class Animal {
    name: string;
}

var animal: Animal; // Animal - type
animal = new Animal();
animal.name = "Beruang";
console.log("Name", animal.name);