// see pg 35

function method<T>(arg: T[]): T[] {
    console.log(arg.length);
    return arg;
}

class CustomPerson extends Array {}

class Person {}

const people: Person[] = [];
const newPerson = new CustomPerson();

method<Person>(people);
method<CustomPerson>(newPerson);