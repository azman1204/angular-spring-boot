// see page 34 - template string
// >tsc string_interpolation.ts || node string_interpolation.js
const nama = "John Doe";
const city = "London";
let str = `
nama saya ${nama}
dan saya tinggal di ${city}
`;
console.log(str);