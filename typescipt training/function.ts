// void - return type , no return
function test(): void {
    const a = 0;
}

// without return type, default is any
function sayHello(name: string) : string {
    return "Hello " + name;
}

let result = sayHello("John Doe");
console.log(result);