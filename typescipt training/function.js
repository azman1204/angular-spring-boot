// void - return type , no return
function test() {
    var a = 0;
}
function sayHello(name) {
    return "Hello " + name;
}

var result = sayHello("John Doe");
console.log(result);
