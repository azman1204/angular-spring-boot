// file ini dipanggil module - sebab ada "export"

export interface Car {
    name: string;
    releaseYear: number;
    info(): string;
}

export class Tesla implements Car {
    name: string;
    releaseYear: number;

    constructor(name: string, releaseYear: number) {
        this.name = name;
        this.releaseYear = releaseYear;
    }

    // optional parameter - ?
    // jika optional dan ada default value, tak perlu letak ?
    getBrandName(prefix?: string, prodYear: number = 2024): string {
        if (prefix)
            return prefix + "  " + this.name + " " + prodYear;
        else
            return this.name + " " + prodYear;
    }

    // note here, tiada keyword "function"
    info(): string {
        return `Name = ${this.name}, Release Year = ${this.releaseYear}`;
    }
}

let tesla = new Tesla("Tesla", 2024);
console.log(tesla.info());
console.log(tesla.getBrandName());
console.log(tesla.getBrandName("USA"));