"use strict";
// file ini dipanggil module - sebab ada "export"
Object.defineProperty(exports, "__esModule", { value: true });
exports.Tesla = void 0;
var Tesla = /** @class */ (function () {
    function Tesla(name, releaseYear) {
        this.name = name;
        this.releaseYear = releaseYear;
    }
    // optional parameter - ?
    // jika optional dan ada default value, tak perlu letak ?
    Tesla.prototype.getBrandName = function (prefix, prodYear) {
        if (prodYear === void 0) { prodYear = 2024; }
        if (prefix)
            return prefix + "  " + this.name + " " + prodYear;
        else
            return this.name + " " + prodYear;
    };
    // note here, tiada keyword "function"
    Tesla.prototype.info = function () {
        return "Name = ".concat(this.name, ", Release Year = ").concat(this.releaseYear);
    };
    return Tesla;
}());
exports.Tesla = Tesla;
var tesla = new Tesla("Tesla", 2024);
console.log(tesla.info());
console.log(tesla.getBrandName());
console.log(tesla.getBrandName("USA"));
