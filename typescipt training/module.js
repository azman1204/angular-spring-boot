"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var interface_class_1 = require("./interface_class");
var S90 = /** @class */ (function () {
    function S90() {
    }
    S90.prototype.info = function () {
        throw new Error("Method not implemented.");
    };
    return S90;
}());
var tesla = new interface_class_1.Tesla("Tesla Model S", 2024);
console.log(tesla.info());
