import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
// import { AppComponent } from './app/app.component';
// import { GuessGameComponent } from './app/guess-game/guess-game.component';
// import { HeroListComponent } from './app/hero-list/hero-list.component';
// import { PostListComponent } from './app/post-list/post-list.component';
import { ListEmpComponent } from './app/emp/list-emp/list-emp.component';

bootstrapApplication(ListEmpComponent, appConfig)
  .catch((err) => console.error(err));
