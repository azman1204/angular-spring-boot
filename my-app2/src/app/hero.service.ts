import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor() { }

  getHeroes() {
    return [
      {id: 1, name:'Superman', group: 'avenger'},
      {id: 2, name:'Spiderman', group: 'avenger'},
      {id: 3, name:'Thanos', group: 'villain'}
    ];
  }
}
