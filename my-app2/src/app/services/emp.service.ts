import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Emp } from '../models/emp';

@Injectable({
  providedIn: 'root'
})
export class EmpService {
  url = "http://localhost:3000/employee";

  constructor(private http: HttpClient) { }

  getAll(): Observable<Emp[]> {
    return this.http.get<Emp[]>(this.url);
  }

  delete(id: string): Observable<Emp> {
    return this.http.delete<Emp>(this.url + "/" + id);
  }

  insert(name: string, email: string): Observable<Emp> {
    let data = {name, email};
    return this.http.post<Emp>(this.url, data);
  }

  update(id: string, name: string, email: string): Observable<Emp> {
    let data = {name, email};
    return this.http.put<Emp>(this.url + "/" + id, data);
  }
}
