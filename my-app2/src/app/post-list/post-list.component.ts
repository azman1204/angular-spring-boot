import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';
import { Post } from '../models/post';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [],
  templateUrl: './post-list.component.html',
  styleUrl: './post-list.component.css'
})
export class PostListComponent implements OnInit {
  posts: Post[] = [];
  constructor(private postService: PostService){}

  // ini cara subscribe ke Observable, once data return akan run function response=>
  ngOnInit(): void {
      this.postService.getAllPosts().subscribe(response => {
        console.log(response);
        this.posts = response;
      }, error => {
        console.error("Ada Error ", error);
      });
  }
}
