import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SummaryPipe } from './summary.pipe';
import { CopyrightDirective } from './copyright.directive';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule, CopyrightDirective, SummaryPipe],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  name = 'xxx';
  heroes = ['Superman', 'Spiderman', 'Batman'];
  heroes2: Hero[] = [
    {id:1, name:'Superman', group: 'g1'},
    {id:2, name:'Spiderman', group: 'g1'},
    {id:3, name:'Batman', group: 'g2'}
  ];
  hero: Hero = {
    id: 1,
    name: 'Superman',
    group: 'avenger'
  }
  today = new Date();
}

export interface Hero {
  id: number, name: string, group: string;
}
