import { Component, OnInit } from '@angular/core';
import { EmpService } from '../../services/emp.service';
import { Emp } from '../../models/emp';
import { FormEmpComponent } from '../form-emp/form-emp.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FormEmpComponent],
  templateUrl: './list-emp.component.html',
  styleUrl: './list-emp.component.css'
})
export class ListEmpComponent implements OnInit {
  employees: Emp[] = [];

  constructor(private empService: EmpService){}

  ngOnInit(): void {
    this.empService.getAll().subscribe(data => {
      console.log(data);
      this.employees = data;
    }, error => console.error("Problem", error));
  }

  doDelete(id: string) {
    this.empService.delete(id).subscribe(data => {
      console.log(data);
    }, error => console.error("Problem", error));
  }
}
