import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { EmpService } from '../../services/emp.service';

@Component({
  selector: 'app-form-emp',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './form-emp.component.html',
  styleUrl: './form-emp.component.css'
})
export class FormEmpComponent {
  id = '';
  name = ''; // two way binding
  email = '';

  constructor(private empService: EmpService){}

  doSubmit() {
    if (this.id === '') {
      this.empService.insert(this.name, this.email).subscribe(data => {
        console.log(data);
      }, error => console.error(error));
    } else {
      this.empService.update(this.id, this.name, this.email).subscribe(data => {
        console.log(data);
      });
    }
  }
}
