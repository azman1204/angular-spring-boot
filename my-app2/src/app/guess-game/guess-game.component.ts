import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './guess-game.component.html',
  styleUrl: './guess-game.component.css'
})
export class GuessGameComponent {
  answer = Math.ceil(Math.random() * 100);
  trials = 0;
  msg = '';
  isWin = false;

  checkAnswer(no: string) {
    console.log(no, this.answer);
    let no2 = parseInt(no);
    if (no2 === this.answer) {
      this.isWin = true;
      this.msg = `YOU WIN, after ${this.trials} trials`;
    } else {
      if (no2 > this.answer)
        this.msg = 'Guess lower';
      else
        this.msg = 'Guess Higher';

      this.trials++;
    }
  }
}
